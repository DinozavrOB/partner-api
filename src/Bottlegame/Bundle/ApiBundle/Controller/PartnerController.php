<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/15/13
 * Time: 4:41 PM
 */

namespace Bottlegame\Bundle\ApiBundle\Controller;

use Bottlegame\Bundle\ApiBundle\Form\Type\PartnerFormType;
use Bottlegame\Bundle\DataBundle\Entity\Partner;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class PartnerController extends FOSRestController
{
    protected $availableAppNames = array('bottlegame', 'photokiss');

    public function getAction($slug)
    {
        $partner = $this->getPartnerBySlug($slug);
        $view = $this->view($partner, 200);
        $view->setFormat('json');

        return $this->handleView($view);
    }

    public function editAction($slug)
    {
        $partner = $this->getPartnerBySlug($slug);
        $view = $this->processForm($partner);

        return $this->handleView($view);
    }

    public function newAction(Request $request)
    {
        $partner = new Partner();
        $view = $this->processForm($partner);

        return $this->handleView($view);
    }

    public function deleteAction($slug)
    {
        $partner = $this->getPartnerBySlug($slug);
        if (!$this->container->get('security.context')->isGranted('ROLE_PARTNER_DELETE')) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager('bottle');
        $em->remove($partner);
        $em->flush();

        $view = $this
            ->view()
            ->setFormat('json')
            ->setStatusCode(204);

        return $this->handleView($view);
    }

    public function getApplicationUrlAction($slug, $app_name = null)
    {
        $urls = array();
        $this->getPartnerBySlug($slug);

        if (!$app_name) {
            foreach ($this->availableAppNames as $name) {
                $urls[$name] = $this->generateAppUrl($slug, $name);
            }
        } elseif (!in_array($app_name, $this->availableAppNames)) {
            throw new ResourceNotFoundException(sprintf('Application name %s not available', $app_name));
        } else {
            $urls[$app_name] = $this->generateAppUrl($slug, $app_name);
        }
        $view = $this->view($urls)
            ->setFormat('json');

        return $this->handleView($view);
    }

    protected function processForm(Partner $partner)
    {
        $statusCode = $partner->isNew() ? 201 : 204;
        $form = $this->createForm(new PartnerFormType(), $partner);
        $data = $this->getRequest()->request->all();
        $children = $form->all();
        $data = array_intersect_key($data, $children);
        $form->submit($data, $partner->isNew());
        $view = $this->view();

        if ($form->isValid()) {
            if ($partner->isNew()) {
                $this->addPrefixToSlug($partner);
            }
            $em = $this->getDoctrine()->getManagerForClass(get_class($partner));
            $em->persist($partner);
            $em->flush();

            if ($statusCode === 201) {
                $view
                    ->setData($partner)
                    ->setHeader(
                        'Location',
                        $this->generateUrl('bottlegame_api_partner_get', array('slug' => $partner->getSlug()))
                    );
            }

        } else {
            $statusCode = 400;
            $view->setData($form);
        }

        $view
            ->setFormat('json')
            ->setStatusCode($statusCode);

        return $view;
    }

    protected function generateAppUrl($partnerSlug, $appName)
    {
        return $this->getParams()['uri'] . sprintf($this->getParams()['pattern'], $partnerSlug, $appName);
    }

    protected function getParnterSlug($slug)
    {
        return $this->getParams()['partner_prefix'] . $slug;
    }

    protected function getParams()
    {
        return $this->container->getParameter('videoserver');
    }

    protected function getPartnerBySlug($slug)
    {
        $partnerSlug = $this->getParnterSlug($slug);
        $em = $this->getDoctrine()->getManager('bottle');
        $repo = $em->getRepository('Bottlegame\Bundle\DataBundle\Entity\Partner');
        $partner = $repo->findOneBySlug($partnerSlug);

        if (!$partner) {
            throw new NotFoundHttpException('partner not found');
        }

        return $partner;
    }

    /**
     * Добавляет префикс
     * @param Partner $partner
     */
    protected function addPrefixToSlug(Partner $partner)
    {
        $partnerSlug = $this->getParnterSlug($partner->getSlug());
        $partner->setSlug($partnerSlug);
    }

}