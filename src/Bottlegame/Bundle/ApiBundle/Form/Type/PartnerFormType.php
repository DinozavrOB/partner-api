<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/18/13
 * Time: 5:57 PM
 */

namespace Bottlegame\Bundle\ApiBundle\Form\Type;


use Bottlegame\Bundle\DataBundle\Entity\Partner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PartnerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('percent')
            ->add('ratio')
            ->add('settings');

        $partner = $builder->getData();
        if ($partner instanceof Partner && $partner->isNew()) {
            $builder->add('slug');
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'csrf_protection' => false
            )
        );

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'partner';
    }


} 