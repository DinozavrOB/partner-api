<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/12/13
 * Time: 2:09 PM
 */

namespace Bottlegame\Bundle\AdminBundle\Admin;

use Bottlegame\Bundle\AdminBundle\Form\DataTransformer\TextToArrayTransformer;
use FOS\OAuthServerBundle\Entity\ClientManager;
use OAuth2\OAuth2;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Admin\FieldDescription;

class ClientAdmin extends Admin
{
    protected function configureFormFields(FormMapper $form)
    {
        $delimiter = ';';
        $transformer = new TextToArrayTransformer($delimiter);
        $form->add('name', 'text', array('required' => false))
            ->add(
                'allowedGrantTypes',
                'choice',
                array(
                    'choices' => array(
                        OAuth2::GRANT_TYPE_AUTH_CODE => 'GRANT_TYPE_AUTH_CODE',
                        OAuth2::GRANT_TYPE_CLIENT_CREDENTIALS => 'GRANT_TYPE_CLIENT_CREDENTIALS',
                        OAuth2::GRANT_TYPE_EXTENSIONS => 'GRANT_TYPE_EXTENSIONS',
                        OAuth2::GRANT_TYPE_IMPLICIT => 'GRANT_TYPE_IMPLICIT',
                        OAuth2::GRANT_TYPE_REFRESH_TOKEN => 'GRANT_TYPE_REFRESH_TOKEN',
                        OAuth2::GRANT_TYPE_REGEXP => 'GRANT_TYPE_REGEXP',
                        OAuth2::GRANT_TYPE_USER_CREDENTIALS => 'GRANT_TYPE_USER_CREDENTIALS'
                    ),
                    'multiple' => true,
                    'required' => true
                )
            )
            ->add(
                $form
                    ->getFormBuilder()
                    ->create(
                        'redirectUris',
                        'text',
                        array('required' => false)
                    )->addViewTransformer($transformer)
            );

        $form->setHelps(
            array('redirectUris' => 'Список урл для редиректов через ' . $delimiter . '')
        );
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->addIdentifier('name');
    }

    public function getNewInstance()
    {
        /* @var $clientManager ClientManager */
        $clientManager = $this->getConfigurationPool()->getContainer()->get('fos_oauth_server.client_manager.default');

        return $clientManager->createClient();
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('id')
            ->add('name')
            ->add('publicId')
            ->add('secret')
            ->add('redirectUris');
    }


} 