<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/12/13
 * Time: 2:39 PM
 */

namespace Bottlegame\Bundle\AdminBundle\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;

class TextToArrayTransformer implements DataTransformerInterface
{
    protected $delimiter;

    public function __construct($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    public function reverseTransform($value)
    {
        if (empty($value)) {
            return array();
        }

        $list = explode($this->delimiter, $value);

        return array_map(
            function ($v) {
                return trim($v);
            },
            $list
        );
    }

    public function transform($value)
    {
        if (empty($value)) {
            return '';
        }

        return implode($this->delimiter, $value);
    }

} 