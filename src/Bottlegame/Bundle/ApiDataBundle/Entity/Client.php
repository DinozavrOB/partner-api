<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/11/13
 * Time: 6:04 PM
 */

namespace Bottlegame\Bundle\ApiDataBundle\Entity;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Client
 * @ORM\Entity
 * @ORM\Table(name="client")
 */
class Client extends BaseClient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var
     * @ORM\Column(type="string", length=20)
     */
    protected $name;

    public function __toString()
    {
        return $this->name ? : 'new';
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}