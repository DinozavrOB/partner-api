<?php
/**
 * Created by PhpStorm.
 * User: shatun
 * Date: 11/15/13
 * Time: 4:54 PM
 */

namespace Bottlegame\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="affiliats", options={"engine"="MyISAM"})
 */
class Partner
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="descript", type="string", length=50, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="latin_name", type="string", length=50, nullable=true, unique=true)
     */
    protected $slug;

    /**
     * @ORM\Column(name="partner_percent", type="integer", length=10, nullable=true)
     */
    protected $percent = 0;

    /**
     * @ORM\Column(name="konvert_koeff", type="float", nullable=true)
     */
    protected $ratio = 0;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $settings;

    public function isNew()
    {
        return $this->id == null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Partner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Partner
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set percent
     *
     * @param $percent
     * @return Partner
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return int
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set ratio
     *
     * @param float $ratio
     * @return Partner
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;

        return $this;
    }

    /**
     * Get ratio
     *
     * @return float
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * Set settings
     *
     * @param json $settings
     * @return Partner
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Get settings
     *
     * @return json
     */
    public function getSettings()
    {
        return $this->settings;
    }
}